<?php


use Magento\Framework\Component\ComponentRegistrar;

$registrar = new ComponentRegistrar();
if ($registrar->getPath(ComponentRegistrar::MODULE, 'AlexStanovoy_TaskManager') === null) {
    ComponentRegistrar::register(ComponentRegistrar::MODULE, 'AlexStanovoy_TaskManager', __DIR__);
}
