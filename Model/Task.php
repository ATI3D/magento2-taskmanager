<?php


namespace AlexStanovoy\TaskManager\Model;

use AlexStanovoy\TaskManager\Api\Data\TaskInterface;
use AlexStanovoy\TaskManager\Api\Data\UserInterface;
use AlexStanovoy\TaskManager\Model\ResourceModel\Task as TaskResource;
use Magento\Framework\Model\AbstractModel;

/**
 * Class Task
 * @package AlexStanovoy\TaskManager\Model
 */
class Task extends AbstractModel implements TaskInterface
{
    /**
     * @var int
     */
    const STATUS_NEW = 0;

    /**
     * @var int
     */
    const STATUS_PROGRESS = 1;

    /**
     * @var int
     */
    const STATUS_COMPLETE = 2;

    /**
     * @var string
     */
    protected $_idFieldName = TaskInterface::TASK_ID; //@codingStandardsIgnoreLine

    /**
     * @inheritdoc
     */
    protected function _construct() //@codingStandardsIgnoreLine
    {
        $this->_init(TaskResource::class);
    }

    /**
     * @return int
     */
    public function getId()
    {
        return parent::getData(TaskInterface::TASK_ID);
    }

    /**
     * @return int
     */
    public function getUserId()
    {
        return $this->getData(TaskInterface::USER_ID);
    }

    /**
     * @param int $userId
     * @return $this
     */
    public function setUserId($userId)
    {
        $this->setData(TaskInterface::USER_ID, $userId);

        return $this;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->getData(TaskInterface::STATUS);
    }

    /**
     * @param int $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->setData(TaskInterface::STATUS, $status);

        return $this;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->getData(TaskInterface::TITLE);
    }

    /**
     * @param string $title
     * @return $this
     */
    public function setTitle(string $title)
    {
        $this->setData(TaskInterface::TITLE, $title);

        return $this;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->getData(TaskInterface::CONTENT);
    }

    /**
     * @param string $content
     * @return $this
     */
    public function setContent(string $content)
    {
        $this->setData(TaskInterface::CONTENT, $content);

        return $this;
    }

    /**
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->getData(TaskInterface::CREATED_AT);
    }

    /**
     * @param string $createdAt
     * @return $this
     */
    public function setCreatedAt(string $createdAt)
    {
        $this->setData(TaskInterface::CREATED_AT, $createdAt);

        return $this;
    }

    /**
     * @return string
     */
    public function getUpdatedAt()
    {
        return $this->getData(TaskInterface::UPDATED_AT);
    }

    /**
     * @param string $updatedAt
     * @return $this
     */
    public function setUpdatedAt(string $updatedAt)
    {
        $this->setData(TaskInterface::UPDATED_AT, $updatedAt);

        return $this;
    }

    /**
     * @return array
     */
    public function getAvailableStatuses()
    {
        return [
            self::STATUS_NEW => __('New'),
            self::STATUS_PROGRESS => __('In progress'),
            self::STATUS_COMPLETE => __('Complete')
        ];
    }
}