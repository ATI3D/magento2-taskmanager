<?php


namespace AlexStanovoy\TaskManager\Model;

use AlexStanovoy\TaskManager\Api\Data\UserInterface;
use AlexStanovoy\TaskManager\Model\ResourceModel\User as UserResource;
use Magento\Framework\Model\AbstractModel;

/**
 * Class User
 * @package AlexStanovoy\TaskManager\Model
 */
class User extends AbstractModel implements UserInterface
{
    
    /**
     * @var string
     */
    protected $_idFieldName = UserInterface::USER_ID; //@codingStandardsIgnoreLine

    /**
     * @inheritdoc
     */
    protected function _construct() //@codingStandardsIgnoreLine
    {
        $this->_init(UserResource::class);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->getData(UserInterface::NAME);
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->setData(UserInterface::NAME, $name);

        return $this;
    }

}