<?php


namespace AlexStanovoy\TaskManager\Model\Task;

use AlexStanovoy\TaskManager\Api\Data\TaskInterface;
use AlexStanovoy\TaskManager\Api\Data\TaskSearchResultInterface;
use AlexStanovoy\TaskManager\Api\Data\TaskSearchResultInterfaceFactory;
use AlexStanovoy\TaskManager\Api\TaskRepositoryInterface;
use AlexStanovoy\TaskManager\Model\ResourceModel\Task as TaskResource;
use AlexStanovoy\TaskManager\Model\ResourceModel\Task\Collection as TaskCollection;
use AlexStanovoy\TaskManager\Model\ResourceModel\Task\CollectionFactory as TaskCollectionFactory;
use AlexStanovoy\TaskManager\Model\Task;
use AlexStanovoy\TaskManager\Model\Task\TaskFactory;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\StateException;

class TaskRepository implements TaskRepositoryInterface
{
    /**
     * @var array
     */
    private $registry = [];

    /**
     * @var TaskResource
     */
    private $taskResource;

    /**
     * @var TaskFactory
     */
    private $taskFactory;

    /**
     * @var TaskCollectionFactory
     */
    private $taskCollectionFactory;

    /**
     * @var TaskSearchResultInterfaceFactory
     */
    private $taskSearchResultFactory;

    /**
     * @var CollectionProcessorInterface
     */
    private $collectionProcessor;

    /**
     * @param TaskResource $taskResource
     * @param TaskFactory $taskFactory
     * @param TaskCollectionFactory $taskCollectionFactory
     * @param TaskSearchResultInterfaceFactory $taskSearchResultFactory
     * @param CollectionProcessorInterface $collectionProcessor
     */
    public function __construct(
        TaskResource $taskResource,
        TaskFactory $taskFactory,
        TaskCollectionFactory $taskCollectionFactory,
        TaskSearchResultInterfaceFactory $taskSearchResultFactory,
        CollectionProcessorInterface $collectionProcessor
    ){
        $this->taskResource = $taskResource;
        $this->taskFactory = $taskFactory;
        $this->taskCollectionFactory = $taskCollectionFactory;
        $this->taskSearchResultFactory = $taskSearchResultFactory;
        $this->collectionProcessor = $collectionProcessor;
    }

    /**
     * @param int $id
     * @return TaskInterface
     * @throws NoSuchEntityException
     */
    public function getById($id)
    {
        $task = $this->taskFactory->create();
        $this->taskResource->load($task, $id);
        if (!$task->getId()) {
            throw new NoSuchEntityException(__('Requested task does not exist'));
        }

        return $task;
    }

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return TaskSearchResultInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria)
    {
        /** @var TaskCollection $collection */
        $collection = $this->taskCollectionFactory->create();
        $this->collectionProcessor->process($searchCriteria, $collection);

        /** @var TaskSearchResultInterface $searchResult */
        $searchResult = $this->taskSearchResultFactory->create();
        $searchResult->setSearchCriteria($searchCriteria);
        $searchResult->setItems($collection->getItems());
        $searchResult->setTotalCount($collection->getSize());

        return $searchResult;
    }

    /**
     * @param TaskInterface $task
     * @return TaskInterface
     * @throws CouldNotSaveException
     */
    public function save(TaskInterface $task)
    {
        try {
            /** @var Task $task */
            $this->taskResource->save($task);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__('Unable to save task #%1', $task->getId()));
        }

        return $task;
    }

    /**
     * @param TaskInterface $task
     * @return bool
     * @throws CouldNotDeleteException
     */
    public function delete(TaskInterface $task)
    {
        try {
            /** @var Task $task */
            $this->taskResource->delete($task);
        } catch (\Exception $e) {
            throw new CouldNotDeleteException(__('Unable to remove task #%1', $task->getId()));
        }

        return true;
    }

    /**
     * @param int $id
     * @return bool
     * @throws CouldNotDeleteException
     * @throws NoSuchEntityException
     */
    public function deleteById($id)
    {
        return $this->delete($this->getById($id));
    }
}