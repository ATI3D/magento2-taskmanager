<?php


namespace AlexStanovoy\TaskManager\Model\Task;

use AlexStanovoy\TaskManager\Api\TaskManagementInterface;
use AlexStanovoy\TaskManager\Api\Data\TaskInterface;
use AlexStanovoy\TaskManager\Model\Task;
use AlexStanovoy\TaskManager\Model\Task\TaskRepository;
use AlexStanovoy\TaskManager\Model\User\UserRepository;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\State\InputMismatchException;

class TaskManagement implements TaskManagementInterface
{
    /**
     * @var TaskRepository
     */
    protected $taskRepository;

    /**
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * @param TaskRepository $taskRepository
     * @param UserRepository $userRepository
     */
    public function __construct(
        TaskRepository $taskRepository,
        UserRepository $userRepository
    ){
        $this->taskRepository = $taskRepository;
        $this->userRepository = $userRepository;
    }

    /**
     * @param int $taskId
     * @param int $status
     * @return Task
     * @throws NoSuchEntityException
     * @throws CouldNotSaveException
     * @throws \Exception
     */
    public function changeStatus($taskId, $status)
    {
        /** @var Task $task */
        $task = $this->taskRepository->getById($taskId);
        $statusKeys = array_keys($task->getAvailableStatuses());
        if (!in_array($status, $statusKeys)) {
            throw new InputMismatchException(
                __('Invalid argument status, available statuses: %1', implode(', ', $statusKeys))
            );
        }
        $task->setStatus($status);
        $this->taskRepository->save($task);
        return $task;
    }

    /**
     * @param int $taskId
     * @param int $userId
     * @return Task
     * @throws NoSuchEntityException
     * @throws CouldNotSaveException
     */
    public function changeUser($taskId, $userId)
    {
        /** @var Task $task */
        $task = $this->taskRepository->getById($taskId);
        $user = $this->userRepository->getById($userId);
        $task->setUserId($user->getId());
        $this->taskRepository->save($task);
        return $task;
    }
}