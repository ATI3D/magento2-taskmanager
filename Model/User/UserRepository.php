<?php


namespace AlexStanovoy\TaskManager\Model\User;

use AlexStanovoy\TaskManager\Api\Data\UserInterface;
use AlexStanovoy\TaskManager\Api\Data\UserSearchResultInterface;
use AlexStanovoy\TaskManager\Api\Data\UserSearchResultInterfaceFactory;
use AlexStanovoy\TaskManager\Api\UserRepositoryInterface;
use AlexStanovoy\TaskManager\Model\ResourceModel\User as UserResource;
use AlexStanovoy\TaskManager\Model\ResourceModel\User\Collection as UserCollection;
use AlexStanovoy\TaskManager\Model\ResourceModel\User\CollectionFactory as UserCollectionFactory;
use AlexStanovoy\TaskManager\Model\User;
use AlexStanovoy\TaskManager\Model\User\UserFactory;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class UserRepository
 * @package AlexStanovoy\TaskManager\Model\User
 */
class UserRepository implements UserRepositoryInterface
{
    /**
     * @var UserResource
     */
    private $userResource;

    /**
     * @var UserFactory
     */
    private $userFactory;

    /**
     * @var UserCollectionFactory
     */
    private $userCollectionFactory;

    /**
     * @var UserSearchResultInterfaceFactory
     */
    private $userSearchResultFactory;

    /**
     * @var CollectionProcessorInterface
     */
    private $collectionProcessor;

    /**
     * @param UserResource $userResource
     * @param UserFactory $userFactory
     * @param UserCollectionFactory $userCollectionFactory
     * @param UserSearchResultInterfaceFactory $userSearchResultFactory
     */
    public function __construct(
        UserResource $userResource,
        UserFactory $userFactory,
        UserCollectionFactory $userCollectionFactory,
        UserSearchResultInterfaceFactory $userSearchResultFactory,
        CollectionProcessorInterface $collectionProcessor
    ){
        $this->userResource = $userResource;
        $this->userFactory = $userFactory;
        $this->userCollectionFactory = $userCollectionFactory;
        $this->userSearchResultFactory = $userSearchResultFactory;
        $this->collectionProcessor = $collectionProcessor;
    }

    /**
     * @param int $id
     * @return UserInterface
     * @throws NoSuchEntityException
     */
    public function getById($id)
    {
        $user = $this->userFactory->create();
        $this->userResource->load($user, $id);
        if (!$user->getId()) {
            throw new NoSuchEntityException(__('Requested user does not exist'));
        }

        return $user;
    }

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return UserSearchResultInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria)
    {
        /** @var UserCollection $collection */
        $collection = $this->userCollectionFactory->create();
        $this->collectionProcessor->process($searchCriteria, $collection);

        /** @var UserSearchResultInterface $searchResult */
        $searchResult = $this->userSearchResultFactory->create();
        $searchResult->setSearchCriteria($searchCriteria);
        $searchResult->setItems($collection->getItems());
        $searchResult->setTotalCount($collection->getSize());
        return $searchResult;
    }

    /**
     * @param UserInterface $user
     * @return UserInterface
     * @throws CouldNotSaveException
     */
    public function save(UserInterface $user)
    {
        try {
            /** @var User $user */
            $this->userResource->save($user);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__('Unable to save user #%1', $user->getId()));
        }
        return $user;
    }

    /**
     * @param UserInterface $user
     * @return bool
     * @throws CouldNotDeleteException
     */
    public function delete(UserInterface $user)
    {
        try {
            /** @var User $user */
            $this->userResource->delete($user);
        } catch (\Exception $e) {
            throw new CouldNotDeleteException(__('Unable to remove user #%1', $user->getId()));
        }

        return true;
    }

    /**
     * @param int $id
     * @return bool
     * @throws CouldNotDeleteException
     * @throws NoSuchEntityException
     */
    public function deleteById($id)
    {
        return $this->delete($this->getById($id));
    }
}