<?php


namespace AlexStanovoy\TaskManager\Model\ResourceModel\Task;

use AlexStanovoy\TaskManager\Model\Task;
use AlexStanovoy\TaskManager\Model\ResourceModel\Task as TaskResource;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Class Collection
 * @package AlexStanovoy\TaskManager\Model\ResourceModel
 */
class Collection extends AbstractCollection
{
    /**
     * @inheritdoc
     */
    protected function _construct() //@codingStandardsIgnoreLine
    {
        $this->_init(Task::class, TaskResource::class);
    }
}