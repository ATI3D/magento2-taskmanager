<?php


namespace AlexStanovoy\TaskManager\Model\ResourceModel\User;

use AlexStanovoy\TaskManager\Model\User;
use AlexStanovoy\TaskManager\Model\ResourceModel\User as UserResource;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Class Collection
 * @package AlexStanovoy\TaskManager\Model\ResourceModel
 */
class Collection extends AbstractCollection
{
    /**
     * @inheritdoc
     */
    protected function _construct() //@codingStandardsIgnoreLine
    {
        $this->_init(User::class, UserResource::class);
    }
}