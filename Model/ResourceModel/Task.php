<?php


namespace AlexStanovoy\TaskManager\Model\ResourceModel;

use AlexStanovoy\TaskManager\Api\Data\TaskInterface;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

/**
 * Class Task
 * @package AlexStanovoy\TaskManager\Model\ResourceModel
 */
class Task extends AbstractDb
{
    /**
     * @var string
     */
    const TABLE_NAME = 'alex_stanovoy_task';

    /**
     * Resource initialization
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(self::TABLE_NAME, TaskInterface::TASK_ID);
    }
}