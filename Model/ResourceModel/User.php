<?php


namespace AlexStanovoy\TaskManager\Model\ResourceModel;

use AlexStanovoy\TaskManager\Api\Data\UserInterface;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class User extends AbstractDb
{
    /**
     * @var string
     */
    const TABLE_NAME = 'alex_stanovoy_task_user';

    /**
     * Resource initialization
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(self::TABLE_NAME, UserInterface::USER_ID);
    }
}