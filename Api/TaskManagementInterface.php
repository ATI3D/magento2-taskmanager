<?php


namespace AlexStanovoy\TaskManager\Api;

use AlexStanovoy\TaskManager\Api\Data\TaskInterface;

/**
 * Interface TaskManagementInterface
 * @package AlexStanovoy\TaskManager\Api
 * @api
 */
interface TaskManagementInterface
{
    /**
     * @param int $taskId
     * @param int $status
     * @return TaskInterface
     */
    public function changeStatus($taskId, $status);

    /**
     * @param int $taskId
     * @param int $userId
     * @return TaskInterface
     */
    public function changeUser($taskId, $userId);
}