<?php


namespace AlexStanovoy\TaskManager\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

/**
 * Interface TaskSearchResultInterface
 * @package AlexStanovoy\TaskManager\Api
 * @api
 */
interface TaskSearchResultInterface extends SearchResultsInterface
{
    /**
     * @return \AlexStanovoy\TaskManager\Api\Data\TaskInterface[];
     */
    public function getItems();

    /**
     * @param \AlexStanovoy\TaskManager\Api\Data\TaskInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}