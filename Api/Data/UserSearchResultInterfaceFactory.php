<?php


namespace AlexStanovoy\TaskManager\Api\Data;

use Magento\Framework\ObjectManagerInterface;
use Temando\Shipping\Api\Data\Delivery\CollectionPointSearchResultInterface;

/**
 * Class UserSearchResultInterfaceFactory
 * @package AlexStanovoy\TaskManager\Api\Data
 * @api
 */
class UserSearchResultInterfaceFactory
{
    /**
     * Object Manager instance
     *
     * @var ObjectManagerInterface
     */
    protected $objectManager = null;

    /**
     * Instance name to create
     *
     * @var string
     */
    protected $instanceName = null;

    /**
     * Factory constructor
     *
     * @param ObjectManagerInterface $objectManager
     * @param string $instanceName
     */
    public function __construct(
        ObjectManagerInterface $objectManager,
        $instanceName = '\\AlexStanovoy\\TaskManager\\Api\\Data\\UserSearchResultInterface'
    ){
        $this->objectManager = $objectManager;
        $this->instanceName = $instanceName;
    }

    /**
     * Create class instance with specified parameters
     *
     * @param array $data
     * @return CollectionPointSearchResultInterface
     */
    public function create(array $data = [])
    {
        return $this->objectManager->create($this->instanceName, $data);
    }
}