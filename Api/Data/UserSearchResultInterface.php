<?php


namespace AlexStanovoy\TaskManager\Api\Data;

use AlexStanovoy\TaskManager\Api\Data\UserInterface;
use Magento\Framework\Api\SearchResultsInterface;

/**
 * Interface UserSearchResultInterface
 * @package AlexStanovoy\TaskManager\Api\Data
 * @api
 */
interface UserSearchResultInterface extends SearchResultsInterface
{
    /**
     * @return UserInterface[];
     */
    public function getItems();

    /**
     * @param UserInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}