<?php


namespace AlexStanovoy\TaskManager\Api\Data;

/**
 * Interface UserInterface
 * @package AlexStanovoy\TaskManager\Api\Data
 * @api
 */
interface UserInterface
{
    /**
     * @var string
     */
    const USER_ID = 'user_id';

    /**
     * @var string
     */
    const NAME = 'name';

    /**
     * @return int
     */
    public function getId();

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id);

    /**
     * @return string
     */
    public function getName();

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name);
}