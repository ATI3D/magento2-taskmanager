<?php


namespace AlexStanovoy\TaskManager\Api\Data;

/**
 * Interface TaskInterface
 * @package AlexStanovoy\TaskManager\Api\Data
 * @api
 */
interface TaskInterface
{
    /**
     * @var string
     */
    const TASK_ID = 'task_id';

    /**
     * @var string
     */
    const USER_ID = 'user_id';

    /**
     * @var string
     */
    const STATUS = 'status';

    /**
     * @var string
     */
    const TITLE = 'title';

    /**
     * @var string
     */
    const CONTENT = 'content';

    /**
     * @var string
     */
    const CREATED_AT = 'created_at';

    /**
     * @var string
     */
    const UPDATED_AT = 'updated_at';

    /**
     * @return int
     */
    public function getId();

    /**
     * @return int
     */
    public function getUserId();

    /**
     * @param int $userId
     * @return $this
     */
    public function setUserId($userId);

    /**
     * @return int
     */
    public function getStatus();

    /**
     * @param int $status
     * @return $this
     */
    public function setStatus($status);

    /**
     * @return string
     */
    public function getTitle();

    /**
     * @param string $title
     * @return $this
     */
    public function setTitle(string $title);

    /**
     * @return string
     */
    public function getContent();

    /**
     * @param string $content
     * @return $this
     */
    public function setContent(string $content);

    /**
     * @return string
     */
    public function getCreatedAt();

    /**
     * @param string $createdAt
     * @return $this
     */
    public function setCreatedAt(string $createdAt);

    /**
     * @return string
     */
    public function getUpdatedAt();

    /**
     * @param string $updatedAt
     * @return $this
     */
    public function setUpdatedAt(string $updatedAt);
}
