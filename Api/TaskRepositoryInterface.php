<?php


namespace AlexStanovoy\TaskManager\Api;

use AlexStanovoy\TaskManager\Api\Data\TaskInterface;
use AlexStanovoy\TaskManager\Api\Data\TaskSearchResultInterface;
use Magento\Framework\Api\SearchCriteriaInterface;

/**
 * Interface TaskRepositoryInterface
 * @package AlexStanovoy\TaskManager\Api
 * @api
 */
interface TaskRepositoryInterface
{
    /**
     * @param int $id
     * @return TaskInterface
     */
    public function getById($id);

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return TaskSearchResultInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria);

    /**
     * @param TaskInterface $task
     * @return TaskInterface
     */
    public function save(TaskInterface $task);

    /**
     * @param TaskInterface $task
     * @return bool
     */
    public function delete(TaskInterface $task);

    /**
     * @param int $id
     * @return bool
     */
    public function deleteById($id);

}
