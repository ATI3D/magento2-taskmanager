<?php


namespace AlexStanovoy\TaskManager\Api;

use AlexStanovoy\TaskManager\Api\Data\UserInterface;
use AlexStanovoy\TaskManager\Api\Data\UserSearchResultInterface;
use Magento\Framework\Api\SearchCriteriaInterface;

/**
 * Interface UserRepositoryInterface
 * @package AlexStanovoy\TaskManager\Api
 * @api
 */
interface UserRepositoryInterface
{
    /**
     * @param int $id
     * @return UserInterface
     */
    public function getById($id);

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return UserSearchResultInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria);

    /**
     * @param UserInterface $user
     * @return UserInterface
     */
    public function save(UserInterface $user);

    /**
     * @param UserInterface $user
     * @return bool
     */
    public function delete(UserInterface $user);

    /**
     * @param int $id
     * @return bool
     */
    public function deleteById($id);
}
