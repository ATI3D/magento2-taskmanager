<?php


namespace AlexStanovoy\TaskManager\Test\Unit\Model\Task;

use AlexStanovoy\TaskManager\Api\Data\TaskInterface;
use AlexStanovoy\TaskManager\Api\Data\TaskSearchResultInterfaceFactory;
use AlexStanovoy\TaskManager\Api\Data\UserInterface;
use AlexStanovoy\TaskManager\Api\Data\UserSearchResultInterfaceFactory;
use AlexStanovoy\TaskManager\Api\TaskManagementInterface;
use AlexStanovoy\TaskManager\Model\ResourceModel\Task as TaskResource;
use AlexStanovoy\TaskManager\Model\ResourceModel\User as UserResource;
use AlexStanovoy\TaskManager\Model\Task;
use AlexStanovoy\TaskManager\Model\User;
use AlexStanovoy\TaskManager\Model\Task\TaskRepository;
use AlexStanovoy\TaskManager\Model\User\UserRepository;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use PHPUnit\Framework\MockObject\MockObject;

class TaskManagementTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var Task\TaskManagement
     */
    protected $taskManagement;

    /**
     * @var Task
     */
    protected $task;

    /**
     * @var TaskInterface
     */
    protected $taskData;

    /**
     * @var User
     */
    protected $user;

    /**
     * @var UserInterface
     */
    protected $userData;

    /**
     * @var TaskRepository
     */
    protected $taskRepository;

    /**
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * @var TaskResource
     */
    protected $taskResource;

    /**
     * @var UserResource
     */
    protected $userResource;

    /**
     * @var CollectionProcessorInterface|MockObject
     */
    private $collectionProcessor;

    /**
     * @inheritDoc
     */
    protected function setUp()
    {
        $this->task = $this->getMockBuilder(Task::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->taskData = $this->getMockBuilder(TaskInterface::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->user = $this->getMockBuilder(User::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->userData = $this->getMockBuilder(UserInterface::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->taskResource = $this->getMockBuilder(TaskResource::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->userResource = $this->getMockBuilder(UserResource::class)
            ->disableOriginalConstructor()
            ->getMock();

        $taskFactory = $this->getMockBuilder(Task\TaskFactory::class)
            ->disableOriginalConstructor()
            ->setMethods(['create'])
            ->getMock();
        $userFactory = $this->getMockBuilder(User\UserFactory::class)
            ->disableOriginalConstructor()
            ->setMethods(['create'])
            ->getMock();
        $taskDataFactory = $this->getMockBuilder(TaskInterface::class)
            ->disableOriginalConstructor()
            ->setMethods(['create'])
            ->getMockForAbstractClass();
        $userDataFactory = $this->getMockBuilder(UserInterface::class)
            ->disableOriginalConstructor()
            ->setMethods(['create'])
            ->getMockForAbstractClass();
        $taskSearchResultFactory = $this->getMockBuilder(TaskSearchResultInterfaceFactory::class)
            ->disableOriginalConstructor()
            ->setMethods(['create'])
            ->getMock();
        $userSearchResultFactory = $this->getMockBuilder(UserSearchResultInterfaceFactory::class)
            ->disableOriginalConstructor()
            ->setMethods(['create'])
            ->getMock();
        $taskCollectionFactory = $this->getMockBuilder(TaskResource\CollectionFactory::class)
            ->disableOriginalConstructor()
            ->setMethods(['create'])
            ->getMock();
        $userCollectionFactory = $this->getMockBuilder(UserResource\CollectionFactory::class)
            ->disableOriginalConstructor()
            ->setMethods(['create'])
            ->getMock();

        $taskFactory->expects($this->any())
            ->method('create')
            ->willReturn($this->task);
        $taskDataFactory->expects($this->any())
            ->method('create')
            ->willReturn($this->taskData);

        $userFactory->expects($this->any())
            ->method('create')
            ->willReturn($this->user);
        $userDataFactory->expects($this->any())
            ->method('create')
            ->willReturn($this->userData);

        $this->collectionProcessor = $this->getMockBuilder(CollectionProcessorInterface::class)
            ->getMockForAbstractClass();

        /**
         * @var Task\TaskFactory $taskFactory
         * @var TaskResource\CollectionFactory $taskCollectionFactory
         * @var TaskSearchResultInterfaceFactory $taskSearchResultFactory
         */
        $taskRepository = new TaskRepository(
            $this->taskResource,
            $taskFactory,
            $taskCollectionFactory,
            $taskSearchResultFactory,
            $this->collectionProcessor
        );

        /**
         * @var User\UserFactory $userFactory
         * @var UserResource\CollectionFactory $userCollectionFactory
         * @var UserSearchResultInterfaceFactory $userSearchResultFactory
         */
        $userRepository = new UserRepository(
            $this->userResource,
            $userFactory,
            $userCollectionFactory,
            $userSearchResultFactory,
            $this->collectionProcessor
        );

        $this->taskManagement = new Task\TaskManagement(
            $taskRepository,
            $userRepository
        );
    }

    /**
     * @return array
     */
    protected function getAvailabilityStatuses()
    {
        return [Task::STATUS_NEW, Task::STATUS_PROGRESS, Task::STATUS_COMPLETE];
    }

    /**
     * @test
     */
    public function testChangeStatus()
    {
        $taskId = 1;
        $status = 1;

        $this->taskResource->expects($this->any())
            ->method('save')
            ->with($this->task)
            ->willReturnSelf();
        $this->task->expects($this->any())
            ->method('getId')
            ->willReturn($taskId);
        $this->task->expects($this->any())
            ->method('getStatus')
            ->willReturn($status);
        $this->task->expects($this->any())
            ->method('load')
            ->with($taskId)
            ->willReturnSelf();
        $this->task->expects($this->any())
            ->method('getAvailableStatuses')
            ->willReturn($this->getAvailabilityStatuses());

        $this->assertEquals($this->task, $this->taskManagement->changeStatus($taskId, $status));
    }

    /**
     * @test
     * @expectedException \Magento\Framework\Exception\CouldNotSaveException
     */
    public function testChangeStatusException()
    {
        $taskId = 1;
        $status = 1;

        $this->taskResource->expects($this->any())
            ->method('save')
            ->with($this->task)
            ->willThrowException(new \Exception());
        $this->task->expects($this->any())
            ->method('getId')
            ->willReturn($taskId);
        $this->task->expects($this->any())
            ->method('getStatus')
            ->willReturn($status);
        $this->task->expects($this->any())
            ->method('load')
            ->with($taskId)
            ->willReturnSelf();
        $this->task->expects($this->any())
            ->method('getAvailableStatuses')
            ->willReturn($this->getAvailabilityStatuses());

        $this->assertEquals($this->task, $this->taskManagement->changeStatus($taskId, $status));
    }

    /**
     * @test
     * @expectedException \Magento\Framework\Exception\State\InputMismatchException
     */
    public function testChangeStatusInputMismatch()
    {
        $taskId = 1;
        $status = 3;

        $this->taskResource->expects($this->any())
            ->method('save')
            ->with($this->task)
            ->willReturnSelf();
        $this->task->expects($this->any())
            ->method('getId')
            ->willReturn($taskId);
        $this->task->expects($this->any())
            ->method('load')
            ->with($taskId)
            ->willReturnSelf();
        $this->task->expects($this->any())
            ->method('getAvailableStatuses')
            ->willReturn($this->getAvailabilityStatuses());

        $this->assertEquals($this->task, $this->taskManagement->changeStatus($taskId, $status));
    }

    /**
     * @test
     * @expectedException \Magento\Framework\Exception\NoSuchEntityException
     */
    public function testChangeStatusTaskNotFound()
    {
        $taskId = 1;
        $status = 1;

        $this->taskResource->expects($this->any())
            ->method('save')
            ->with($this->task)
            ->willReturnSelf();
        $this->task->expects($this->any())
            ->method('getId')
            ->willReturn(null);
        $this->task->expects($this->any())
            ->method('load')
            ->with($taskId)
            ->willReturnSelf();

        $this->assertEquals($this->task, $this->taskManagement->changeStatus($taskId, $status));
    }

    /**
     * @test
     */
    public function testChangeUser()
    {
        $taskId = 1;
        $userId = 1;

        $this->taskResource->expects($this->any())
            ->method('save')
            ->with($this->task)
            ->willReturnSelf();
        $this->task->expects($this->any())
            ->method('getId')
            ->willReturn($taskId);
        $this->task->expects($this->any())
            ->method('getUserId')
            ->willReturn($userId);
        $this->user->expects($this->any())
            ->method('getId')
            ->willReturn($userId);
        $this->task->expects($this->any())
            ->method('load')
            ->with($taskId)
            ->willReturnSelf();
        $this->user->expects($this->any())
            ->method('load')
            ->with($userId)
            ->willReturnSelf();

        $this->assertEquals($this->task, $this->taskManagement->changeUser($taskId, $userId));
    }

    /**
     * @test
     * @expectedException \Magento\Framework\Exception\NoSuchEntityException
     */
    public function testChangeUserUserNotFound()
    {
        $taskId = 1;
        $userId = 3;

        $this->taskResource->expects($this->any())
            ->method('save')
            ->with($this->task)
            ->willReturnSelf();
        $this->task->expects($this->any())
            ->method('getId')
            ->willReturn($taskId);
        $this->task->expects($this->any())
            ->method('getUserId')
            ->willReturn($userId);
        $this->user->expects($this->any())
            ->method('getId')
            ->willReturn(null);
        $this->task->expects($this->any())
            ->method('load')
            ->with($taskId)
            ->willReturnSelf();
        $this->user->expects($this->any())
            ->method('load')
            ->with($userId)
            ->willReturnSelf();

        $this->assertEquals($this->task, $this->taskManagement->changeUser($taskId, $userId));
    }

    /**
     * @test
     * @expectedException \Magento\Framework\Exception\NoSuchEntityException
     */
    public function testChangeUserTaskNotFound()
    {
        $taskId = 3;
        $userId = 1;

        $this->taskResource->expects($this->any())
            ->method('save')
            ->with($this->task)
            ->willReturnSelf();
        $this->task->expects($this->any())
            ->method('getId')
            ->willReturn(null);
        $this->task->expects($this->any())
            ->method('getUserId')
            ->willReturn($userId);
        $this->user->expects($this->any())
            ->method('getId')
            ->willReturn($userId);
        $this->task->expects($this->any())
            ->method('load')
            ->with($taskId)
            ->willReturnSelf();
        $this->user->expects($this->any())
            ->method('load')
            ->with($userId)
            ->willReturnSelf();

        $this->assertEquals($this->task, $this->taskManagement->changeUser($taskId, $userId));
    }
}