<?php


namespace AlexStanovoy\TaskManager\Test\Unit\Model\Task;

use AlexStanovoy\TaskManager\Api\Data\TaskInterface;
use AlexStanovoy\TaskManager\Api\Data\TaskSearchResultInterface;
use AlexStanovoy\TaskManager\Api\Data\TaskSearchResultInterfaceFactory;
use AlexStanovoy\TaskManager\Model\ResourceModel\Task as TaskResource;
use AlexStanovoy\TaskManager\Model\Task;
use AlexStanovoy\TaskManager\Model\Task\TaskRepository;

use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use PHPUnit\Framework\MockObject\MockObject;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Reflection\DataObjectProcessor;

/**
 * Class TaskRepositoryTest
 * @package AlexStanovoy\TaskManager\Test\Unit
 */
class TaskRepositoryTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var TaskRepository
     */
    protected $repository;

    /**
     * @var TaskResource
     */
    protected $taskResource;

    /**
     * @var Task
     */
    protected $task;

    /**
     * @var TaskInterface
     */
    protected $taskData;

    /**
     * @var TaskSearchResultInterface
     */
    protected $taskSearchResult;

    /**
     * @var TaskResource\Collection
     */
    protected $collection;

    /**
     * @var MockObject|DataObjectHelper
     */
    protected $dataHelper;

    /**
     * @var MockObject|DataObjectProcessor
     */
    protected $dataObjectProcessor;

    /**
     * @var CollectionProcessorInterface|MockObject
     */
    private $collectionProcessor;

    /**
     * @inheritDoc
     */
    protected function setUp()
    {
        $this->taskResource = $this->getMockBuilder(TaskResource::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->dataObjectProcessor = $this->getMockBuilder(DataObjectProcessor::class)
            ->disableOriginalConstructor()
            ->getMock();

        $taskFactory = $this->getMockBuilder(Task\TaskFactory::class)
            ->disableOriginalConstructor()
            ->setMethods(['create'])
            ->getMock();
        $taskDataFactory = $this->getMockBuilder(TaskInterface::class)
            ->disableOriginalConstructor()
            ->setMethods(['create'])
            ->getMockForAbstractClass();
        $taskSearchResultFactory = $this->getMockBuilder(TaskSearchResultInterfaceFactory::class)
            ->disableOriginalConstructor()
            ->setMethods(['create'])
            ->getMock();
        $collectionFactory = $this->getMockBuilder(TaskResource\CollectionFactory::class)
            ->disableOriginalConstructor()
            ->setMethods(['create'])
            ->getMock();

        $this->task = $this->getMockBuilder(Task::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->taskData = $this->getMockBuilder(TaskInterface::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->taskSearchResult = $this->getMockBuilder(TaskSearchResultInterface::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->collection = $this->getMockBuilder(TaskResource\Collection::class)
            ->disableOriginalConstructor()
            ->setMethods(['getSize', 'setCurPage', 'setPageSize', 'load', 'addOrder'])
            ->getMock();

        $taskFactory->expects($this->any())
            ->method('create')
            ->willReturn($this->task);
        $taskDataFactory->expects($this->any())
            ->method('create')
            ->willReturn($this->taskData);
        $taskSearchResultFactory->expects($this->any())
            ->method('create')
            ->willReturn($this->taskSearchResult);
        $collectionFactory->expects($this->any())
            ->method('create')
            ->willReturn($this->collection);

        $this->dataHelper = $this->getMockBuilder(DataObjectHelper::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->collectionProcessor = $this->getMockBuilder(CollectionProcessorInterface::class)
            ->getMockForAbstractClass();

        /**
         * @var Task\TaskFactory $taskFactory
         * @var TaskResource\CollectionFactory $collectionFactory
         * @var TaskSearchResultInterfaceFactory $taskSearchResultFactory
         */
        $this->repository = new TaskRepository(
            $this->taskResource,
            $taskFactory,
            $collectionFactory,
            $taskSearchResultFactory,
            $this->collectionProcessor
        );
    }

    /**
     * @test
     */
    public function testSave()
    {
        $this->taskResource->expects($this->once())
            ->method('save')
            ->with($this->task)
            ->willReturnSelf();
        $this->assertEquals($this->task, $this->repository->save($this->task));
    }

    /**
     * @test
     */
    public function testDeleteById()
    {
        $taskId = 1;

        $this->task->expects($this->any())
            ->method('getId')
            ->willReturn(true);

        $this->task->expects($this->any())
            ->method('load')
            ->with($taskId)
            ->willReturnSelf();

        $this->task->expects($this->any())
            ->method('delete')
            ->with($this->task)
            ->willReturnSelf();

        $this->assertTrue($this->repository->deleteById($taskId));
    }

    /**
     * @test
     * @expectedException \Magento\Framework\Exception\CouldNotSaveException
     */
    public function testSaveException()
    {
        $this->taskResource->expects($this->once())
            ->method('save')
            ->with($this->task)
            ->willThrowException(new \Exception());
        $this->assertEquals($this->task, $this->repository->save($this->task));
    }

    /**
     * @test
     * @expectedException \Magento\Framework\Exception\CouldNotDeleteException
     */
    public function testDeleteException()
    {
        $this->taskResource->expects($this->once())
            ->method('delete')
            ->with($this->task)
            ->willThrowException(new \Exception());
        $this->repository->delete($this->task);
    }

    /**
     * @test
     * @expectedException \Magento\Framework\Exception\NoSuchEntityException
     */
    public function testGetByIdException()
    {
        $taskId = 1;

        $this->task->expects($this->any())
            ->method('getId')
            ->willReturn(false);

        $this->task->expects($this->any())
            ->method('load')
            ->with($taskId)
            ->willReturnSelf();

        $this->repository->getById($taskId);
    }

    /**
     * @test
     * @throws \Exception
     */
    public function testGetList()
    {
        $total = 10;

        /** @var \Magento\Framework\Api\SearchCriteriaInterface $criteria */
        $criteria = $this->getMockBuilder(\Magento\Framework\Api\SearchCriteriaInterface::class)->getMock();

        $this->collection->addItem($this->task);
        $this->collection->expects($this->once())
            ->method('getSize')
            ->willReturn($total);

        $this->collectionProcessor->expects($this->once())
            ->method('process')
            ->with($criteria, $this->collection)
            ->willReturnSelf();

        $this->taskSearchResult->expects($this->once())
            ->method('setSearchCriteria')
            ->with($criteria)
            ->willReturnSelf();

        $this->taskSearchResult->expects($this->once())
            ->method('setTotalCount')
            ->with($total)
            ->willReturnSelf();

        $this->taskSearchResult->expects($this->once())
            ->method('setItems')
            ->with([$this->task])
            ->willReturnSelf();

        $this->assertEquals($this->taskSearchResult, $this->repository->getList($criteria));
    }
}