<?php


namespace AlexStanovoy\TaskManager\Test\Unit\Model\User;

use AlexStanovoy\TaskManager\Api\Data\UserInterface;
use AlexStanovoy\TaskManager\Api\Data\UserSearchResultInterface;
use AlexStanovoy\TaskManager\Api\Data\UserSearchResultInterfaceFactory;
use AlexStanovoy\TaskManager\Model\ResourceModel\User as UserResource;
use AlexStanovoy\TaskManager\Model\User;
use AlexStanovoy\TaskManager\Model\User\UserRepository;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Exception\NoSuchEntityException;
use PHPUnit\Framework\MockObject\MockObject;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Reflection\DataObjectProcessor;

/**
 * Class UserRepositoryTest
 * @package AlexStanovoy\TaskManager\Test\Unit\Model\User
 */
class UserRepositoryTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var UserRepository
     */
    protected $repository;

    /**
     * @var UserResource
     */
    protected $userResource;

    /**
     * @var User
     */
    protected $user;

    /**
     * @var UserInterface
     */
    protected $userData;

    /**
     * @var UserSearchResultInterface
     */
    protected $userSearchResult;

    /**
     * @var UserResource\Collection
     */
    protected $collection;

    /**
     * @var MockObject|DataObjectHelper
     */
    protected $dataHelper;

    /**
     * @var MockObject|DataObjectProcessor
     */
    protected $dataObjectProcessor;

    /**
     * @var CollectionProcessorInterface|MockObject
     */
    private $collectionProcessor;

    protected function setUp()
    {
        $this->userResource = $this->getMockBuilder(UserResource::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->dataObjectProcessor = $this->getMockBuilder(DataObjectProcessor::class)
            ->disableOriginalConstructor()
            ->getMock();

        $userFactory = $this->getMockBuilder(User\UserFactory::class)
            ->disableOriginalConstructor()
            ->setMethods(['create'])
            ->getMock();
        $userDataFactory = $this->getMockBuilder(UserInterface::class)
            ->disableOriginalConstructor()
            ->setMethods(['create'])
            ->getMockForAbstractClass();
        $userSearchResultFactory = $this->getMockBuilder(UserSearchResultInterfaceFactory::class)
            ->disableOriginalConstructor()
            ->setMethods(['create'])
            ->getMock();
        $collectionFactory = $this->getMockBuilder(UserResource\CollectionFactory::class)
            ->disableOriginalConstructor()
            ->setMethods(['create'])
            ->getMock();

        $this->user = $this->getMockBuilder(User::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->userData = $this->getMockBuilder(UserInterface::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->userSearchResult = $this->getMockBuilder(UserSearchResultInterface::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->collection = $this->getMockBuilder(UserResource\Collection::class)
            ->disableOriginalConstructor()
            ->setMethods(['getSize', 'setCurPage', 'setPageSize', 'load', 'addOrder'])
            ->getMock();

        $userFactory->expects($this->any())
            ->method('create')
            ->willReturn($this->user);
        $userDataFactory->expects($this->any())
            ->method('create')
            ->willReturn($this->userData);
        $userSearchResultFactory->expects($this->any())
            ->method('create')
            ->willReturn($this->userSearchResult);
        $collectionFactory->expects($this->any())
            ->method('create')
            ->willReturn($this->collection);

        $this->dataHelper = $this->getMockBuilder(DataObjectHelper::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->collectionProcessor = $this->getMockBuilder(CollectionProcessorInterface::class)
            ->getMockForAbstractClass();

        /**
         * @var User\UserFactory $userFactory
         * @var UserResource\CollectionFactory $collectionFactory
         * @var UserSearchResultInterfaceFactory $userSearchResultFactory
         */
        $this->repository = new UserRepository(
            $this->userResource,
            $userFactory,
            $collectionFactory,
            $userSearchResultFactory,
            $this->collectionProcessor
        );
    }

    /**
     * @test
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     */
    public function testSave()
    {
        $this->userResource->expects($this->once())
            ->method('save')
            ->with($this->user)
            ->willReturnSelf();
        $this->assertEquals($this->user, $this->repository->save($this->user));
    }

    /**
     * @test
     * @throws \Magento\Framework\Exception\CouldNotDeleteException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function testDeleteById()
    {
        $userId = 1;

        $this->user->expects($this->any())
            ->method('getId')
            ->willReturn(true);

        $this->user->expects($this->any())
            ->method('load')
            ->with($userId)
            ->willReturnSelf();

        $this->user->expects($this->any())
            ->method('delete')
            ->with($this->user)
            ->willReturnSelf();

        $this->assertTrue($this->repository->deleteById($userId));
    }

    /**
     * @test
     * @expectedException \Magento\Framework\Exception\CouldNotSaveException
     */
    public function testSaveException()
    {
        $this->userResource->expects($this->once())
            ->method('save')
            ->with($this->user)
            ->willThrowException(new \Exception());
        $this->assertEquals($this->user, $this->repository->save($this->user));
    }

    /**
     * @test
     * @expectedException \Magento\Framework\Exception\CouldNotDeleteException
     */
    public function testDeleteException()
    {
        $this->userResource->expects($this->once())
            ->method('delete')
            ->with($this->user)
            ->willThrowException(new \Exception());
        $this->repository->delete($this->user);
    }

    /**
     * @test
     * @expectedException \Magento\Framework\Exception\NoSuchEntityException
     */
    public function testGetByIdException()
    {
        $userId = 1;

        $this->user->expects($this->any())
            ->method('getId')
            ->willReturn(false);

        $this->user->expects($this->any())
            ->method('load')
            ->with($userId)
            ->willReturnSelf();

        $this->repository->getById($userId);
    }

    /**
     * @test
     * @throws \Exception
     */
    public function testGetList()
    {
        $total = 10;

        /** @var \Magento\Framework\Api\SearchCriteriaInterface $criteria */
        $criteria = $this->getMockBuilder(\Magento\Framework\Api\SearchCriteriaInterface::class)->getMock();

        $this->collection->addItem($this->user);
        $this->collection->expects($this->once())
            ->method('getSize')
            ->willReturn($total);

        $this->collectionProcessor->expects($this->once())
            ->method('process')
            ->with($criteria, $this->collection)
            ->willReturnSelf();

        $this->userSearchResult->expects($this->once())
            ->method('setSearchCriteria')
            ->with($criteria)
            ->willReturnSelf();

        $this->userSearchResult->expects($this->once())
            ->method('setTotalCount')
            ->with($total)
            ->willReturnSelf();

        $this->userSearchResult->expects($this->once())
            ->method('setItems')
            ->with([$this->user])
            ->willReturnSelf();

        $this->assertEquals($this->userSearchResult, $this->repository->getList($criteria));
    }

}